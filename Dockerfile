FROM node:4.4.5
# FROM node:4-slim
# FROM mhart/alpine-node:4

MAINTAINER Juffee Sason

# RUN apk add --no-cache make gcc g++ python

RUN npm cache clean -f

ENV NODE_ENV development

ENV DOCKER mongodb

# install from shadow .package.json (notice the dot)
# ADD .package.json /tmp/package.json
# RUN cd /tmp && npm install

# install from actual package.json
# ADD package.json /tmp/package.json
# RUN cd /tmp && npm install

RUN mkdir -p /usr/src/app

ADD ./package.json /usr/src/app/package.json

WORKDIR /usr/src/app

RUN npm install



ADD . /usr/src/app

ENV PATH="${PATH}:/usr/src/app/node_modules/.bin"

# RUN export 

# RUN npm -g install gulp
# RUN gulp build

EXPOSE 8080 9000 3000 27017

# CMD [ "node", "dist/server/app.js" ]