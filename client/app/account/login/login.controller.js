'use strict';

export default class LoginController {

  /*@ngInject*/
  constructor(Auth, $state) {
    this.Auth = Auth;
    this.$state = $state;
    this.errors = {};
  }

  login(form) {
    this.submitted = true;

    if(form.$valid) {
      this.Auth.login({
        email: this.user.email,
        password: this.user.password
      })
        .then(user => {
          // Logged in, redirect to home
          if(user.role === 'agency') {
            this.$state.go('agency.dashboard');
            return;
          }
          // Logged in, redirect to home
          if(user.role === 'admin') {
            this.$state.go('admin.dashboard');
            return;
          }
          this.$state.go('landing');
        })
        .catch(err => {
          this.errors.login = err.message;
        });
    }
  }
}
