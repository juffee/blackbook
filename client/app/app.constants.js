'use strict';

import angular from 'angular';

export default angular.module('blackbookApp.constants', [])
  .constant('appConfig', require('../../server/config/environment/shared'))
  .constant('cities', require('../../server/config/environment/shared-cities'))
  .name;
