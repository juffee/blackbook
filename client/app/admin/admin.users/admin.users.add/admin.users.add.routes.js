'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.users.add', {
      url: '/admin/users/add',
      template: '<admin-users-add></admin-users-add>'
    });
}
