'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.users.add.routes';

export class AdminUsersAddComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('blackbookApp.admin.users.add', [uiRouter])
  .config(routes)
  .component('admin.users.add', {
    template: require('./admin.users.add.html'),
    controller: AdminUsersAddComponent,
    controllerAs: 'admin.users.addCtrl'
  })
  .name;
