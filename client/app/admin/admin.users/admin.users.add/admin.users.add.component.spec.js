'use strict';

describe('Component: AdminUsersAddComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.users.add'));

  var AdminUsersAddComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminUsersAddComponent = $componentController('admin.users.add', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
