'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.users.routes';

export class AdminUsersComponent {
  /*@ngInject*/
  constructor() {
    'ngInject';
  }
}

export default angular.module('blackbookApp.admin.users', [uiRouter])
  .config(routes)
  .component('adminUsers', {
    template: require('./admin.users.html'),
    bindings: {allUsers: '<', currentUser: '<', pendingClaims: '<', approvedClaims: '<'},
    controller: AdminUsersComponent,
    controllerAs: 'vm'
  })
  .name;
