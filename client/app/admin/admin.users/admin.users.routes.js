'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.users', {
      url: '/users',
      template: '<admin-users all-users="$resolve.allUsers"  current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"></admin-users>',
      authenticate: 'admin.viewAllUsers'
    });
}