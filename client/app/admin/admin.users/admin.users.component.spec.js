'use strict';

describe('Component: AdminUsersComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.users'));

  var AdminUsersComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminUsersComponent = $componentController('admin.users', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
