'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.users.edit', {
      url: '/admin/users/edit',
      template: '<admin-users-edit></admin-users-edit>'
    });
}
