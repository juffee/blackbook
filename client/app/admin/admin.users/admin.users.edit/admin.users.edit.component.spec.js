'use strict';

describe('Component: AdminUsersEditComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.users.edit'));

  var AdminUsersEditComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminUsersEditComponent = $componentController('admin.users.edit', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
