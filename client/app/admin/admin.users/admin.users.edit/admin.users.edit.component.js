'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.users.edit.routes';

export class AdminUsersEditComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('blackbookApp.admin.users.edit', [uiRouter])
  .config(routes)
  .component('admin.users.edit', {
    template: require('./admin.users.edit.html'),
    controller: AdminUsersEditComponent,
    controllerAs: 'admin.users.editCtrl'
  })
  .name;
