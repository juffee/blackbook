'use strict';

// TODO: Move template to another html file

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin', {
      url: '/admin',
      abstract: true,
      template: '<admin all-users="$resolve.allUsers" current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"><ui-view/></admin>',
      resolve: {
        allUsers(User) {
          return User.query();
        },
        currentUser(Auth) {
          return Auth.getCurrentUser();
        },
        pendingClaims(ClaimResource) {
          return ClaimResource.getClaimsPending({});
        },
        approvedClaims(ClaimResource) {
          return ClaimResource.getClaimsApproved({});
        }
      },
    });
}







// 'use strict';

// export default function routes($stateProvider) {
//   'ngInject';

//   $stateProvider.state('admin', {
//     url: '/admin',
//     template: require('./admin.html'),
//     controller: 'AdminController',
//     controllerAs: 'admin',
//     authenticate: 'admin.viewAll'
//   });
// }
