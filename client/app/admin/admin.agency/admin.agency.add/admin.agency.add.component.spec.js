'use strict';

describe('Component: AdminAgencyAddComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.agency.add'));

  var AdminAgencyAddComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminAgencyAddComponent = $componentController('admin.agency.add', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
