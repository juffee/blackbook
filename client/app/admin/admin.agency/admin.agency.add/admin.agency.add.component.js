'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.agency.add.routes';

export class AdminAgencyAddComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('blackbookApp.admin.agency.add', [uiRouter])
  .config(routes)
  .component('admin.agency.add', {
    template: require('./admin.agency.add.html'),
    controller: AdminAgencyAddComponent,
    controllerAs: 'admin.agency.addCtrl'
  })
  .name;
