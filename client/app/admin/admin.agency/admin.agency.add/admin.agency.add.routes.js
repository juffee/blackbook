'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.agency.add', {
      url: '/admin/agency/add',
      template: '<admin-agency-add></admin-agency-add>'
    });
}
