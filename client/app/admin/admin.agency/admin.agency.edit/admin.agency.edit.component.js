'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.agency.edit.routes';

export class AdminAgencyEditComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('blackbookApp.admin.agency.edit', [uiRouter])
  .config(routes)
  .component('admin.agency.edit', {
    template: require('./admin.agency.edit.html'),
    controller: AdminAgencyEditComponent,
    controllerAs: 'admin.agency.editCtrl'
  })
  .name;
