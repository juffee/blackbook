'use strict';

describe('Component: AdminAgencyEditComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.agency.edit'));

  var AdminAgencyEditComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminAgencyEditComponent = $componentController('admin.agency.edit', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
