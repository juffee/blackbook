'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.agency.edit', {
      url: '/admin/agency/edit',
      template: '<admin-agency-edit></admin-agency-edit>'
    });
}
