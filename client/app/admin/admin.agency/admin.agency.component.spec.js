'use strict';

describe('Component: AdminAgencyComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.agency'));

  var AdminAgencyComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminAgencyComponent = $componentController('admin.agency', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
