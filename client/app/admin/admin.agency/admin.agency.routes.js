'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.agency', {
      url: '/admin/agencies',
      template: '<admin-agency></admin-agency>'
    });
}
