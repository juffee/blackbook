'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.agency.routes';

export class AdminAgencyComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('blackbookApp.admin.agency', [uiRouter])
  .config(routes)
  .component('admin.agency', {
    template: require('./admin.agency.html'),
    controller: AdminAgencyComponent,
    controllerAs: 'admin.agencyCtrl'
  })
  .name;
