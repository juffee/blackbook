'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import { ClaimResource } from '../../components/claim/claim.resource';

import adminUsers from './admin.users/admin.users.component';
import adminDashboard from './admin.dashboard/admin.dashboard.component';
// import adminReports from './admin.reports/admin.reports.component';
// import adminPendingClaims from './admin.pending-claims/admin.pending-claims.component';

import routes from './admin.routes';

export default angular.module('blackbookApp.admin', [uiRouter, adminUsers, adminDashboard])
  .config(routes)
  .factory('ClaimResource', ClaimResource)
  .name;
