'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './admin.dashboard.routes';

export class AdminDashboardComponent {
  /*@ngInject*/
  constructor() {
    this.pendingClaimsListOptions = {
      role: 'admin',
      type: 'Pending'
    };
    this.approvedClaimsListOptions = {
      role: 'admin',
      type: 'Approved'
    };
  }
}

export default angular.module('blackbookApp.admin.dashboard', [uiRouter])
  .config(routes)
  .component('adminDashboard', {
    template: require('./admin.dashboard.html'),
    bindings: {allUsers: '<', currentUser: '<', pendingClaims: '<', approvedClaims: '<'},
    controller: AdminDashboardComponent,
    controllerAs: 'vm'
  })
  .name;
