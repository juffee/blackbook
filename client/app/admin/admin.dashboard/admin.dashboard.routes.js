'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admin.dashboard', {
      url: '/admin/dashboard',
      template: '<admin-dashboard all-users="$resolve.allUsers"  current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"></admin-dashboard>',
      authenticate: 'admin.viewDashboard'
    });
}
