'use strict';

describe('Component: AdminDashboardComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.admin.dashboard'));

  var AdminDashboardComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminDashboardComponent = $componentController('admin.dashboard', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
