'use strict';

describe('Component: AgencyDashboardComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.agency.dashboard'));

  var AgencyDashboardComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AgencyDashboardComponent = $componentController('agency.dashboard', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
