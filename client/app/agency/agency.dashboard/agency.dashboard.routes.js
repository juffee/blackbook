'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('agency.dashboard', {
      url: '/dashboard',
      template: '<agency-dashboard current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"></agency-dashboard>',      
      authenticate: 'agency.viewAll'
    });
}
