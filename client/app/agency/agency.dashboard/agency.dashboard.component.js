'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './agency.dashboard.routes';

export class AgencyDashboardComponent {
  /*@ngInject*/
  constructor() {
    'ngInject';

    this.pendingClaimsListOptions = {
      role: 'agency',
      type: 'Pending'
    };
    this.approvedClaimsListOptions = {
      role: 'agency',
      type: 'Approved'
    };
  }
}

export default angular.module('blackbookApp.agency.dashboard', [uiRouter])
  .config(routes)
  .component('agencyDashboard', {
    template: require('./agency.dashboard.html'),
    bindings: {currentUser: '<', pendingClaims: '=', approvedClaims: '<'},
    controller: AgencyDashboardComponent,
    controllerAs: 'vm'
  })
  .name;
