'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('agency.reports', {
      url: '/reports',
      template: '<agency-reports current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"></agency-reports>'
    });
}
