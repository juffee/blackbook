'use strict';

describe('Component: AgencyReportsComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.agency.reports'));

  var AgencyReportsComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AgencyReportsComponent = $componentController('agency.reports', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
