'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './agency.reports.routes';

export class AgencyReportsComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('blackbookApp.agency.reports', [uiRouter])
  .config(routes)
  .component('agencyReports', {
    template: require('./agency.reports.html'),
    controller: AgencyReportsComponent,
    controllerAs: 'vm'
  })
  .name;
