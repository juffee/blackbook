'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('agency.new-entry', {
      url: '/new-entry',
      template: '<agency-new-entry current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"></agency-new-entry>'
    });
}
// authenticate: 'agency.newEntry'

