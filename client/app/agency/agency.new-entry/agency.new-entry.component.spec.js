'use strict';

describe('Component: AgencyNewEntryComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.agency.new-entry'));

  var AgencyNewEntryComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AgencyNewEntryComponent = $componentController('agency.new-entry', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
