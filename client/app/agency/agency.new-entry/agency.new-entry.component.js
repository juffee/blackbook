'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './agency.new-entry.routes';

export class AgencyNewEntryComponent {
  /*@ngInject*/
  constructor(PersonResource, ClaimResource, appConfig, cities, $window, $state) {
    'ngInject';
    this.Person = PersonResource;
    this.Claim = ClaimResource;
    this.$window = $window;
    this.$state = $state;    

    this.dateOptions = appConfig.dateOptions;
    // TODO: Move this to dateoptions?
    this.altInputFormats = ['MM/dd/yyyy', 'MM d, yyyy', 'MMMM d, yyyy', 'MMM d, yyyy'];
    this.popupopened = false;

    this.person = {};

    this.addPersonFlag = true;
    this.selectNameFlag = false;

    this.claimStatuses = appConfig.claimStatuses;
    this.countries = appConfig.countries;
    this.cities = cities;
    this.currencies = appConfig.currencies;
    this.peril = appConfig.peril;

    this.loadingClaims = false;
    this.validationPatterns = appConfig.validationPatterns;

    this.submitting = false;
  }

  searchPerson(name) {
    return this.Person.searchPerson({id: name}).$promise.then(function(result) {
      return result;
    });
  }

  onSelectName($model) {
    this.addPersonFlag = false;
    this.selectNameFlag = true;
    this.person = $model;
    this.person.birthdate = new Date($model.birthdate);
    this.loadingClaims = true;
    this.Claim.getClaimsOfPerson({id: $model._id}).$promise.then(claims => {
      this.loadingClaims = false;
      this.person.claims = claims;
    });
  }

  /**
   * Either create new person or add claims for that person
   */
  onSubmit() {
    this.submitting = true;
    this.addPersonForm.$setSubmitted();
    this.addClaimForm.$setSubmitted();

    if(this.addPersonForm.$valid && this.addClaimForm.$valid) {
      // Prepare agency
      this.claim.agency = this.currentUser.agency._id;

      // Prepare claim
      this.claim.status = 'Pending';      

      if(this.addPersonFlag) {
        // Create/Save new person
        console.log('Save new person.');
        this.Person.save(this.person).$promise.then(savedPerson => {
          console.log(savedPerson);
          // Prepare person
          this.claim.person = savedPerson._id;
          console.log('Save new claim.');
          this.Claim.save(this.claim).$promise.then(savedClaim => {
            console.log(savedClaim);
            this.$window.alert('New claim and person saved');
            this.$state.go('agency.dashboard', {}, {reload: true});
            this.submitting = false;
          });
        });
      } else {
        // Prepare person
        this.claim.person = this.person._id;
        // Add claim
        console.log('Save new claim for person.');
        this.Claim.save(this.claim).$promise.then(savedClaim => {
          console.log(savedClaim);
          this.$window.alert('New claim saved');
          this.$state.go('agency.dashboard', {}, {reload: true});
          this.submitting = false;
        });
      }
    } else {
      this.submitting = false;
      this.$window.alert('Invalid form.');
      console.log('Invalid Forms.');
    }
  }
}

export default angular.module('blackbookApp.agency.new-entry', [uiRouter, 'ui.bootstrap'])
  .config(routes)
  .component('agencyNewEntry', {
    template: require('./agency.new-entry.html'),
    bindings: {currentUser: '<', pendingClaims: '=', approvedClaims: '<'},
    controller: AgencyNewEntryComponent,
    controllerAs: 'vm'
  })
  .name;
