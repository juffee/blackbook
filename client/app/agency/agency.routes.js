'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('agency', {
      url: '/agency',
      abstract: true,
      template: '<agency current-user="$resolve.currentUser" pending-claims="$resolve.pendingClaims" approved-claims="$resolve.approvedClaims"><ui-view/></agency>',
      resolve: {
        currentUser(Auth) {
          return Auth.getCurrentUser();
        },
        pendingClaims(ClaimResource, currentUser) {
          return ClaimResource.getClaimsOfAgencyPending({id: currentUser.agency._id});
        },
        approvedClaims(ClaimResource, currentUser) {
          return ClaimResource.getClaimsOfAgencyApproved({id: currentUser.agency._id});
        }
      },
    });
}
