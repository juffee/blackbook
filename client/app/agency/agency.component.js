'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import { ClaimResource } from '../../components/claim/claim.resource';
import { PersonResource } from '../../components/person/person.resource';

import agencyNewEntry from './agency.new-entry/agency.new-entry.component';
import agencyDashboard from './agency.dashboard/agency.dashboard.component';
import agencyReports from './agency.reports/agency.reports.component';

import routes from './agency.routes';

export default angular.module('blackbookApp.agency', [uiRouter, agencyNewEntry, agencyDashboard, agencyReports])
  .config(routes)
  .factory('ClaimResource', ClaimResource)
  .factory('PersonResource', PersonResource)
  .name;
