'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('search-person', {
      url: '/search',
      template: '<search-person></search-person>'
    });
}
