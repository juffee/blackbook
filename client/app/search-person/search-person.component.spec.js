'use strict';

describe('Component: SearchPersonComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.search-person'));

  var SearchPersonComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    SearchPersonComponent = $componentController('search-person', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
