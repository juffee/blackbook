'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './search-person.routes';

export class SearchPersonComponent {
  /*@ngInject*/
  constructor(PersonResource, ClaimResource) {
    'ngInject';
    this.Person = PersonResource;
    this.Claim = ClaimResource;
    this.selectNameFlag = false;
  }

  searchPerson(name) {
    return this.Person.searchPerson({id: name}).$promise.then(function(result) {
      return result;
    });
  }

  onSelectName($model) {    
    this.selectNameFlag = true;
    this.person = $model;
    this.person.birthdate = new Date($model.birthdate);
    this.loadingClaims = true;
    this.Claim.getClaimsOfPerson({id: $model._id}).$promise.then(claims => {
      this.loadingClaims = false;
      this.person.claims = claims;
    });
  }
}

export default angular.module('blackbookApp.search-person', [uiRouter])
  .config(routes)
  .component('searchPerson', {
    template: require('./search-person.html'),
    controller: SearchPersonComponent,
    controllerAs: 'vm'
  })
  .name;
