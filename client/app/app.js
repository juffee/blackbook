'use strict';

import angular from 'angular';
import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';
import 'angular-socket-io';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngMessages from 'angular-messages';
// import ngValidationMatch from 'angular-validation-match';


import {
  routeConfig
} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';

import sidebar from '../components/sidebar/sidebar.component';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import claim from '../components/claim/claim.module';

import confirm from '../components/confirm/confirm.directive';
import backbutton from '../components/util/backbutton.directive';

import main from './main/main.component';
import landing from './landing/landing.component';
import admin from './admin/admin.component';
import agency from './agency/agency.component';
import search from './search-person/search-person.component';

import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';

import './app.scss';

angular.module('blackbookApp', [ngCookies, ngResource, ngSanitize, ngAnimate, 'btford.socket-io', uiRouter,
    uiBootstrap, ngMessages, _Auth,
    account, admin, navbar, sidebar, confirm, backbutton, footer, claim,
    main, landing, agency, search,
    constants, socket, util
  ])
  .config(routeConfig)
  .run(function($rootScope, $location, Auth) {
    'ngInject';
    // Redirect to login if route requires auth and you're not logged in

    $rootScope.$on('$stateChangeStart', function(event, next) {
      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['blackbookApp'], {
      strictDi: true
    });
  });
