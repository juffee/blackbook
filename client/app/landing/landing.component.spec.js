'use strict';

describe('Component: LandingComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.landing'));

  var LandingComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    LandingComponent = $componentController('landing', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
