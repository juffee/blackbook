'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './landing.routes';

export class LandingComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';    
  }
}

export default angular.module('blackbookApp.landing', [uiRouter])
  .config(routes)
  .component('landing', {
    template: require('./landing.html'),
    controller: LandingComponent,
    controllerAs: 'landingCtrl'
  })
  .name;
