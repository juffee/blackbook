'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('landing', {
      url: '/',
      template: '<landing></landing>'
    });
}
