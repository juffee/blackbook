'use strict';
const angular = require('angular');

export default angular.module('directives.backbutton', [])
  .directive('backbutton', function($window) {
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element) {
        element.on('click', function() {
          $window.history.back();
        });
      }
    };
  })
  .name;
