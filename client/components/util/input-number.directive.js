// DID NOT WORK
'use strict';
const angular = require('angular');

export default angular.module('directives.input', [])
  .directive('input', function() {
    return {
      restrict: 'E',
      require: '?ngModel',
      link(scope, element, attrs, ngModel) {        
        if(typeof attrs.type !== 'undefined'
          && attrs.type === 'number'
          && ngModel
        ) {
          ngModel.$formatters.push(function(modelValue) {
            console.log('fpush')
            return Number(modelValue);
          });

          ngModel.$parsers.push(function(viewValue) {
            console.log('ppush')
            return Number(viewValue);
          });
        }
      }
    };
  })
  .name;
