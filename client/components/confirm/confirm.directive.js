'use strict';
const angular = require('angular');

export default angular.module('directives.confirm', [])
  .directive('confirm', function() {
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('click', function(e) {
          var message = attrs.confirm ? attrs.confirm : 'Are you sure?';
          if(message && !confirm(message)) {
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    };
  })
  .name;
