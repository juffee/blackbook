'use strict';

export function PersonResource($resource) {
  'ngInject';

  return $resource('/api/persons/:id/:controller', {
    id: '@_id'
  }, {
    searchPerson: {
      method: 'GET',
      params: {
        controller: 'search',
        id: '@id'
      },
      isArray: true
    }
  });
}
