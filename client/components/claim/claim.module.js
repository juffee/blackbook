'use strict';
const angular = require('angular');

import listClaims from './list-claims/list-claims.component';
import viewClaim from './view-claim/view-claim.component';
import viewClaimRoute from './view-claim-route/view-claim.component';

export default angular.module('blackbookApp.claim', [listClaims, viewClaim, viewClaimRoute])
  .name;
