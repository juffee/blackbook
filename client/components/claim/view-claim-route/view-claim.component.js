'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './view-claim.routes';

export class ViewClaimRouteComponent {
  /*@ngInject*/
  constructor() {
    'ngInject';
  }
}

export default angular.module('blackbookApp.claim.view-claim-route', [uiRouter])
  .config(routes)
  .component('viewClaimRoute', {
    template: require('./view-claim.html'),
    bindings: {claim: '<'},
    controller: ViewClaimRouteComponent,
    controllerAs: 'vm'
  })
  .name;
