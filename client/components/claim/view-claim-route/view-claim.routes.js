'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('view-claim', {
      url: '/claim/view/:id',
      template: '<view-claim-route claim="$resolve.claim"></view-claim-route>',
      resolve: {
        claim($stateParams, ClaimResource) {
          return ClaimResource.get({id: $stateParams.id});
        }
      },
    });
}
