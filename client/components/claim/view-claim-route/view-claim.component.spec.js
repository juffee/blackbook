'use strict';

describe('Component: ViewClaimComponent', function() {
  // load the controller's module
  beforeEach(module('blackbookApp.claim.view-claim'));

  var ViewClaimComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    ViewClaimComponent = $componentController('view-claim', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
