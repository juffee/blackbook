'use strict';
const angular = require('angular');

export class viewClaimComponent {
  /*@ngInject*/
  constructor() {
    'ngInject';
  }
}

export default angular.module('blackbookApp.claim.view-claim', [])
  .component('viewClaim', {
    template: require('./view-claim.template.html'),
    bindings: { claim: '=' },
    controller: viewClaimComponent,
    controllerAs: 'vm'
  })
  .name;
