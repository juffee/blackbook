'use strict';
const angular = require('angular');

export class listClaimsComponent {
  /*@ngInject*/
  constructor(ClaimResource, $state, $window) {
    'ngInject';
    this.Claim = ClaimResource;
    this.$state = $state;
    this.$window = $window;

    if(this.options === undefined) {
      this.options = {
        role: 'agency',
        type: 'Pending'
      };
    }
  }

  accept(_id) {
    this.Claim.acceptClaim({id: _id}).$promise.then(claim => {
      if(claim) {
        this.$state.go(this.$state.current, {}, {reload: true, notify: true});
      } else {
        this.$window.alert('Sorry but there seemed to have been an error. Please try again or contact the admin.');
        this.$state.go(this.$state.current, {}, {reload: true, notify: true});
      }
    });
  }

  dismiss(_id) {
    this.Claim.dismissClaim({id: _id}).$promise.then(claim => {
      if(claim) {
        this.$state.go(this.$state.current, {}, {reload: true, notify: true});
      } else {
        this.$window.alert('Sorry but there seemed to have been an error. Please try again or contact the admin.');
        this.$state.go(this.$state.current, {}, {reload: true, notify: true});
      }
    });
  }
}

export default angular.module('blackbookApp.claim.list-claims', [])
  .component('listClaims', {
    template: require('./list-claims.template.html'),
    bindings: { claims: '=', options: '<' },
    controller: listClaimsComponent,
    controllerAs: 'vm'
  })
  .name;
