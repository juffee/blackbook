'use strict';

export function ClaimResource($resource) {
  'ngInject';

  return $resource('/api/claims/:id/:controller', {
    id: '@_id'
  }, {
    getClaimsOfPerson: {
      method: 'GET',
      params: {
        controller: 'person',
        id: '@id'
      },
      isArray: true
    },
    getClaimsOfAgencyApproved: {
      method: 'GET',
      url: '/api/claims/:id/agency/approved',
      params: {
        id: '@id'
      },
      isArray: true
    },
    getClaimsOfAgencyPending: {
      method: 'GET',
      url: '/api/claims/:id/agency/pending',
      params: {
        id: '@id'
      },
      isArray: true
    },
    getClaimsApproved: {
      method: 'GET',
      url: '/api/claims/approved',
      isArray: true
    },
    getClaimsPending: {
      method: 'GET',
      url: '/api/claims/pending',
      isArray: true
    },
    acceptClaim: {
      method: 'PATCH',
      params: {
        controller: 'accept',
        id: '@id'
      }
    },
    dismissClaim: {
      method: 'PATCH',
      params: {
        controller: 'dismiss',
        id: '@id'
      }
    }
  });
}
