/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

 // TODO: FIX THIS MESS MY GAWD.

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import Agency from '../api/agency/agency.model';
import Person from '../api/person/person.model';
import Claim from '../api/claim/claim.model';

Thing.find({}).remove()
  .then(() => {
    Thing.create({
      name: 'Development Tools',
      info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
            + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
            + 'Stylus, Sass, and Less.'
    }, {
      name: 'Server and Client integration',
      info: 'Built with a powerful and fun stack: MongoDB, Express, '
            + 'AngularJS, and Node.'
    }, {
      name: 'Smart Build System',
      info: 'Build system ignores `spec` files, allowing you to keep '
            + 'tests alongside code. Automatic injection of scripts and '
            + 'styles into your index.html'
    }, {
      name: 'Modular Structure',
      info: 'Best practice client and server structures allow for more '
            + 'code reusability and maximum scalability'
    }, {
      name: 'Optimized Build',
      info: 'Build process packs up your templates as a single JavaScript '
            + 'payload, minifies your scripts/css/images, and rewrites asset '
            + 'names for caching.'
    }, {
      name: 'Deployment Ready',
      info: 'Easily deploy your app to Heroku or Openshift with the heroku '
            + 'and openshift subgenerators'
    });
  });

var clearUsers = function() {
  return User.find({}).remove();
};

var clearPersons = function() {
  return Person.find({}).remove();
};

var clearClaims = function() {
  return Claim.find({}).remove();
};

//TODO: Remove permissions sa users

var createAgencyUsers1 = function(agency) {
  return User.create({
    provider: 'local',
    role: 'agency',
    name: 'Agency User 1',
    email: 'agency@example.com',
    password: 'agency',
    agency: agency._id
  });
};

var createAgencyUsers2 = function(agency) {
  return User.create({
    provider: 'local',
    role: 'agency',
    name: 'Agency User 2',
    email: 'agency2@example.com',
    password: 'agency',
    agency: agency._id
  });
};

var createAdmin = function() {
  return User.create({
    provider: 'local',
    role: 'admin',
    name: 'Admin User',
    email: 'admin@example.com',
    password: 'admin'
  }).then(() => {
    console.log('Create User Admin.')
  });
};

var createPersonsAndClaims1 = function(agency) {
  console.log('[createPersonsAndClaims1]');
  return Person.create({
    firstname: 'Leonila',
    lastname: 'Lopez',
    middlename: 'Domingo',
    colnumber: '252800',
    birthdate: new Date('Jan 1, 1985')
  })
  .then(person => {
    console.log('Person created.');
    return Claim.create({
      person: person._id,
      status: 'Approved',
      claimstatus: 'Paid - exgratia',
      peril: 'Repatriation Cost',
      description: 'Per OFW written statement she was repartriated due to lack of foods and 1 month unpaid salary.',
      filedate: new Date('August 27, 2014'),
      lossdate: new Date('July 24, 2014'),
      country: 'Qatar',
      city: 'Doha',
      claimcurrency: 'USD',
      claimamount: 450.77,
      paidamountphp: 19349.75,
      paidamountusd: 450.77,
      paiddate: new Date('9/12/2014'),
      remarks: '',
      agency: agency._id
    });
  })
  .then(() => {
    console.log('Creating Person.');
    return Person.create({
      firstname: 'Mariano',
      lastname: 'Corpuz',
      middlename: 'Versoza',
      colnumber: '261713',
      birthdate: new Date('Jan 1, 1985')
    })
    .then(person => {
      console.log('Person created.');
      return Claim.create({
        person: person._id,
        status: 'Approved',
        claimstatus: 'Paid - exgratia',
        peril: 'Repatriation Cost',
        description: 'Per OFW written statement she was repartriated because he did not passed in his driving exam. So his employer decided to send him back in the Philippines.',
        filedate: new Date('August 27, 2014'),
        lossdate: new Date('July 24, 2014'),
        country: 'Qatar',
        city: 'Doha',
        claimcurrency: 'QAR',
        claimamount: 1330.00,
        paidamountphp: 16382.94,
        paidamountusd: 365.02,
        paiddate: new Date('11/5/2014'),
        remarks: '',
        agency: agency._id
      });
    });
  })
  .then(() => {
    console.log('Creating Person.');
    return Person.create({
      firstname: 'Norma',
      lastname: 'B',
      middlename: 'Akmad',
      colnumber: '277901',
      birthdate: new Date('Jan 1, 1985')
    })
    .then(person => {
      console.log('Person created.');
      return Claim.create({
        person: person._id,
        status: 'Approved',
        claimstatus: 'Paid',
        peril: 'Repatriation Cost',
        description: 'Per OFW written statement she was repartriated due to lack of food, unpaid salary and maltreatment by her employer.',
        filedate: new Date('November 17, 2014'),
        lossdate: new Date('October 24, 2014'),
        country: 'Kuwait',
        city: 'Kuwait City',
        claimcurrency: 'USD',
        claimamount: 320.00,
        paidamountphp: 14243.20,
        paidamountusd: 320.00,
        paiddate: new Date('1/7/2015'),
        remarks: '',
        agency: agency._id
      });
    });
  })
  .then(() => {
    console.log('Person and claims created.');
    return Person.create({
      firstname: 'Maricar',
      lastname: 'Sason',
      middlename: 'Miranda',
      colnumber: '1234568',
      birthdate: new Date('October 26, 1985')
    }).then(person => {
      console.log('creating claim...');
      return Claim.create({
        person: person._id,
        status: 'Approved',
        claimstatus: 'Paid - exgratia',
        peril: 'Repatriation Cost',
        description: 'Claim Testing 2',
        filedate: new Date('January 1, 2003'),
        lossdate: new Date('January 12, 2004'),
        country: 'US',
        claimcurrency: 'QAR',
        claimamount: 1750.00,
        paidamountphp: 21689.50,
        paidamountusd: 480.53,
        paiddate: new Date('10/02/2015'),
        remarks: 'Lorem Ipsum. Girl j-pop franchise range-rover computer neural engine geodesic Tokyo jeans.',
        agency: agency._id
      }, {
        person: person._id,
        status: 'Pending',
        claimstatus: 'Paid - exgratia',
        peril: 'Money Claims',
        description: 'no reasons yet',
        filedate: new Date('08/17/2015'),
        lossdate: new Date('07/05/2015'),
        country: 'US',
        claimcurrency: 'USD',
        claimamount: 310.00,
        paidamountphp: 13950.00,
        paidamountusd: 310.00,
        paiddate: new Date('10/02/2015'),
        remarks: 'Lorem Ipsum. Girl j-pop franchise range-rover computer neural engine geodesic Tokyo jeans.',
        agency: agency._id
      });
    });
  });
};

var createPersonsAndClaims2 = function(agency) {
  console.log('[createPersonsAndClaims1]');
  return Person.create({
    firstname: 'Katherine',
    lastname: 'Zaldivar',
    middlename: 'C',
    colnumber: '309461',
    birthdate: new Date('Jan 1, 1985')
  })
  .then(person => {
    console.log('Person created.');
    return Claim.create({
      person: person._id,
      status: 'Approved',
      claimstatus: 'Paid - exgratia',
      peril: 'Repatriation Cost',
      description: 'Per OFW written statement she was terminated for the reasons she doesnt know how to do manicure/pedicure and not trustworthy said by her employer but she refuted this accusations. According to her it is hard to work with an environment where there is a favoritism and shes not comfortable with that. But still she tried to cope up whatever task to undertake in the salon buy they were not satisfied.',
      filedate: new Date('January 13, 2015'),
      lossdate: new Date('December 5, 2014'),
      country: 'Libya',
      city: 'Tripoli',
      claimcurrency: 'PHP',
      claimamount: 15000,
      paidamountphp: 340.18,
      paidamountusd: 15000,
      paiddate: new Date('2/11/2015'),
      remarks: '',
      agency: agency._id
    });
  })
  .then(() => {
    console.log('Creating Person.');
    return Person.create({
      firstname: 'Ana Maria',
      lastname: 'Estoesta',
      middlename: 'Estuto',
      colnumber: '317803',
      birthdate: new Date('Jan 1, 1985')
    })
    .then(person => {
      console.log('Person created.');
      return Claim.create({
        person: person._id,
        status: 'Approved',
        claimstatus: 'Paid - exgratia',
        peril: 'Repatriation Cost',
        description: 'Per OFW Written statement she decided to return back to the Philippines due to heavy work load, lack of rest & lack of food.',
        filedate: new Date('February 20, 2015'),
        lossdate: new Date('February 11, 2015'),
        country: 'Kuwait',
        city: 'Kuwait City',
        claimcurrency: 'USD',
        claimamount: 320.00,
        paidamountphp: 14400,
        paidamountusd: 320.00,
        paiddate: new Date('3/11/2015'),
        remarks: '',
        agency: agency._id
      });
    });
  })
  .then(() => {
    console.log('Creating Person.');
    return Person.create({
      firstname: 'Juvelyn',
      lastname: 'Andrade',
      middlename: 'Abad',
      colnumber: '272858',
      birthdate: new Date('Jan 1, 1985')
    })
    .then(person => {
      console.log('Person created.');
      return Claim.create({
        person: person._id,
        status: 'Approved',
        claimstatus: 'Paid',
        peril: 'Repatriation Cost',
        description: 'Per OFW Written Statement she was repartriated due to maltreatment by her employer & lack of foods.',
        filedate: new Date('November 17, 2014'),
        lossdate: new Date('October 24, 2014'),
        country: 'Kuwait',
        city: 'Kuwait City',
        claimcurrency: 'QAR',
        claimamount: 1750.00,
        paidamountphp: 20650.00,
        paidamountusd: 468.38,
        paiddate: new Date('3/11/2015'),
        remarks: '',
        agency: agency._id
      });
    });
  });
};

var createAgency = function() {
  Agency.find({}).remove()
    .then(() => {
      console.log('CLEARED Agencies.');
      console.log('Creating Agency.');
      return Agency.create({
        name: 'United Global Agency'
      })
      .then(agency => {
        console.log('Agency 1 created.');
        return createAgencyUsers1(agency)
          .then(() => {
            console.log('Users for Agency 1 created.');
            return createPersonsAndClaims1(agency).then(() => {
              console.log('Persons and Claims for Agency 1 created.');
              return null;
            });
          });
      });
    })
    .then(() => {
      console.log('Creating Next Agency.');
      return Agency.create({
        name: 'ABC Agency'
      })
      .then(agency => {
        console.log('Agency 2 created.');

        return createAgencyUsers2(agency)
          .then(() => {
            console.log('Users for Agency 2 created.');
            return createPersonsAndClaims2(agency).then(() => {
              console.log('Persons and Claims for Agency 2 created.');
              return null;
            });
          });

      });
    });
};

clearUsers().then(() => {
  console.log('CLEARED Users');
  clearPersons().then(() => {
    console.log('CLEARED Persons');
    clearClaims().then(() => {
      console.log('CLEARED Claims');
      createAgency();
      createAdmin();
    });
  });
});
