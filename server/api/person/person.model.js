'use strict';

import mongoose from 'mongoose';

// import ClaimSchema from '../claim/claim.model';

var PersonSchema = new mongoose.Schema({
  firstname: {type: String, index: true},
  lastname: {type: String, index: true},
  middlename: {type: String, index: true},
  birthdate: Date,

  colnumber: String,

  // claims: [ClaimSchema.schema],

  photos: [String],

  info: String,
  active: Boolean
});

PersonSchema.index({lastname: 'text', firstname: 'text', middlename: 'text'});

export default mongoose.model('Person', PersonSchema);
