'use strict';

var express = require('express');
var controller = require('./person.controller');

var router = express.Router();

router.get('/', controller.index);
// router.get('/:name/search', controller.searchNameExact);
router.get('/:name/search', controller.searchNameWithPartial);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);

module.exports = router;
