'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var claimCtrlStub = {
  index: 'claimCtrl.index',
  show: 'claimCtrl.show',
  create: 'claimCtrl.create',
  upsert: 'claimCtrl.upsert',
  patch: 'claimCtrl.patch',
  destroy: 'claimCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var claimIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './claim.controller': claimCtrlStub
});

describe('Claim API Router:', function() {
  it('should return an express router instance', function() {
    expect(claimIndex).to.equal(routerStub);
  });

  describe('GET /api/claims', function() {
    it('should route to claim.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'claimCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/claims/:id', function() {
    it('should route to claim.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'claimCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/claims', function() {
    it('should route to claim.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'claimCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/claims/:id', function() {
    it('should route to claim.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'claimCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/claims/:id', function() {
    it('should route to claim.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'claimCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/claims/:id', function() {
    it('should route to claim.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'claimCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
