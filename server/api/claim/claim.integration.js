'use strict';

var app = require('../..');
import request from 'supertest';

var newClaim;

describe('Claim API:', function() {
  describe('GET /api/claims', function() {
    var claims;

    beforeEach(function(done) {
      request(app)
        .get('/api/claims')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          claims = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(claims).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/claims', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/claims')
        .send({
          name: 'New Claim',
          info: 'This is the brand new claim!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newClaim = res.body;
          done();
        });
    });

    it('should respond with the newly created claim', function() {
      expect(newClaim.name).to.equal('New Claim');
      expect(newClaim.info).to.equal('This is the brand new claim!!!');
    });
  });

  describe('GET /api/claims/:id', function() {
    var claim;

    beforeEach(function(done) {
      request(app)
        .get(`/api/claims/${newClaim._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          claim = res.body;
          done();
        });
    });

    afterEach(function() {
      claim = {};
    });

    it('should respond with the requested claim', function() {
      expect(claim.name).to.equal('New Claim');
      expect(claim.info).to.equal('This is the brand new claim!!!');
    });
  });

  describe('PUT /api/claims/:id', function() {
    var updatedClaim;

    beforeEach(function(done) {
      request(app)
        .put(`/api/claims/${newClaim._id}`)
        .send({
          name: 'Updated Claim',
          info: 'This is the updated claim!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedClaim = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedClaim = {};
    });

    it('should respond with the original claim', function() {
      expect(updatedClaim.name).to.equal('New Claim');
      expect(updatedClaim.info).to.equal('This is the brand new claim!!!');
    });

    it('should respond with the updated claim on a subsequent GET', function(done) {
      request(app)
        .get(`/api/claims/${newClaim._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let claim = res.body;

          expect(claim.name).to.equal('Updated Claim');
          expect(claim.info).to.equal('This is the updated claim!!!');

          done();
        });
    });
  });

  describe('PATCH /api/claims/:id', function() {
    var patchedClaim;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/claims/${newClaim._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Claim' },
          { op: 'replace', path: '/info', value: 'This is the patched claim!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedClaim = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedClaim = {};
    });

    it('should respond with the patched claim', function() {
      expect(patchedClaim.name).to.equal('Patched Claim');
      expect(patchedClaim.info).to.equal('This is the patched claim!!!');
    });
  });

  describe('DELETE /api/claims/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/claims/${newClaim._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when claim does not exist', function(done) {
      request(app)
        .delete(`/api/claims/${newClaim._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
