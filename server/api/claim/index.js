'use strict';

var express = require('express');
var controller = require('./claim.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id/person', controller.getClaimsOfPerson);
router.get('/:id/agency/approved', controller.getClaimsOfAgencyApproved);
router.get('/:id/agency/pending', controller.getClaimsOfAgencyPending);
router.get('/approved', controller.getClaimsApproved);
router.get('/pending', controller.getClaimsPending);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.patch('/:id/accept', controller.acceptClaim);
router.patch('/:id/dismiss', controller.dismissClaim);
router.delete('/:id', controller.destroy);

module.exports = router;
