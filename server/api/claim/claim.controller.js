/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/claims              ->  index
 * POST    /api/claims              ->  create
 * GET     /api/claims/:id          ->  show
 * PUT     /api/claims/:id          ->  upsert
 * PATCH   /api/claims/:id          ->  patch
 * DELETE  /api/claims/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Claim from './claim.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log('[Error]', err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of Claims
export function index(req, res) {
  return Claim.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Claims
export function getClaimsOfPerson(req, res) {
  return Claim.find({person: req.params.id}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Claims
export function getClaimsOfAgencyApproved(req, res) {
  return Claim.find({agency: req.params.id, status: 'Approved'})
    .populate('agency')
    .populate('person')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Claims
export function getClaimsOfAgencyPending(req, res) {
  return Claim.find({agency: req.params.id, status: 'Pending'})
    .populate('agency')
    .populate('person')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Claims
export function getClaimsApproved(req, res) {
  return Claim.find({status: 'Approved'})
    .populate('agency')
    .populate('person')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Claims
export function getClaimsPending(req, res) {
  return Claim.find({status: 'Pending'})
    .populate('agency')
    .populate('person')
    .exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Claim from the DB
export function show(req, res) {
  return Claim.findById(req.params.id)
    .populate('agency')
    .populate('person')
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Claim in the DB
export function create(req, res) {
  return Claim.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Claim in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Claim.findOneAndUpdate(req.params.id, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Claim in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Claim.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Claim in the DB
export function acceptClaim(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Claim.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates([{op: 'replace', path: '/status', value: 'Approved' }]))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Claim in the DB
export function dismissClaim(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Claim.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates([{op: 'replace', path: '/status', value: 'Dismissed' }]))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Claim from the DB
export function destroy(req, res) {
  return Claim.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
