'use strict';

import mongoose from 'mongoose';

var ClaimSchema = new mongoose.Schema({
  status: String,
  filedate: Date,
  claimstatus: String,
  lossdate: Date,
  description: String,
  peril: String,
  country: String,
  city: String,
  claimcurrency: String,
  claimamount: Number,
  paidamountphp: Number,
  paidamountusd: Number,
  remarks: String,
  agency: {type: mongoose.Schema.Types.ObjectId, ref: 'Agency'},
  person: {type: mongoose.Schema.Types.ObjectId, ref: 'Person'},
  active: Boolean
});

export default mongoose.model('Claim', ClaimSchema);
