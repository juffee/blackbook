/**
 * Claim model events
 */

'use strict';

import {EventEmitter} from 'events';
import Claim from './claim.model';
var ClaimEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ClaimEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Claim.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    ClaimEvents.emit(event + ':' + doc._id, doc);
    ClaimEvents.emit(event, doc);
  };
}

export default ClaimEvents;
