'use strict';

import mongoose from 'mongoose';

var AgencySchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Agency', AgencySchema);
