'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var agencyCtrlStub = {
  index: 'agencyCtrl.index',
  show: 'agencyCtrl.show',
  create: 'agencyCtrl.create',
  upsert: 'agencyCtrl.upsert',
  patch: 'agencyCtrl.patch',
  destroy: 'agencyCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var agencyIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './agency.controller': agencyCtrlStub
});

describe('Agency API Router:', function() {
  it('should return an express router instance', function() {
    expect(agencyIndex).to.equal(routerStub);
  });

  describe('GET /api/agencys', function() {
    it('should route to agency.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'agencyCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/agencys/:id', function() {
    it('should route to agency.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'agencyCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/agencys', function() {
    it('should route to agency.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'agencyCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/agencys/:id', function() {
    it('should route to agency.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'agencyCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/agencys/:id', function() {
    it('should route to agency.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'agencyCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/agencys/:id', function() {
    it('should route to agency.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'agencyCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
