'use strict';

var app = require('../..');
import request from 'supertest';

var newAgency;

describe('Agency API:', function() {
  describe('GET /api/agencys', function() {
    var agencys;

    beforeEach(function(done) {
      request(app)
        .get('/api/agencys')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          agencys = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(agencys).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/agencys', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/agencys')
        .send({
          name: 'New Agency',
          info: 'This is the brand new agency!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newAgency = res.body;
          done();
        });
    });

    it('should respond with the newly created agency', function() {
      expect(newAgency.name).to.equal('New Agency');
      expect(newAgency.info).to.equal('This is the brand new agency!!!');
    });
  });

  describe('GET /api/agencys/:id', function() {
    var agency;

    beforeEach(function(done) {
      request(app)
        .get(`/api/agencys/${newAgency._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          agency = res.body;
          done();
        });
    });

    afterEach(function() {
      agency = {};
    });

    it('should respond with the requested agency', function() {
      expect(agency.name).to.equal('New Agency');
      expect(agency.info).to.equal('This is the brand new agency!!!');
    });
  });

  describe('PUT /api/agencys/:id', function() {
    var updatedAgency;

    beforeEach(function(done) {
      request(app)
        .put(`/api/agencys/${newAgency._id}`)
        .send({
          name: 'Updated Agency',
          info: 'This is the updated agency!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedAgency = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedAgency = {};
    });

    it('should respond with the original agency', function() {
      expect(updatedAgency.name).to.equal('New Agency');
      expect(updatedAgency.info).to.equal('This is the brand new agency!!!');
    });

    it('should respond with the updated agency on a subsequent GET', function(done) {
      request(app)
        .get(`/api/agencys/${newAgency._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let agency = res.body;

          expect(agency.name).to.equal('Updated Agency');
          expect(agency.info).to.equal('This is the updated agency!!!');

          done();
        });
    });
  });

  describe('PATCH /api/agencys/:id', function() {
    var patchedAgency;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/agencys/${newAgency._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Agency' },
          { op: 'replace', path: '/info', value: 'This is the patched agency!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedAgency = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedAgency = {};
    });

    it('should respond with the patched agency', function() {
      expect(patchedAgency.name).to.equal('Patched Agency');
      expect(patchedAgency.info).to.equal('This is the patched agency!!!');
    });
  });

  describe('DELETE /api/agencys/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/agencys/${newAgency._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when agency does not exist', function(done) {
      request(app)
        .delete(`/api/agencys/${newAgency._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
