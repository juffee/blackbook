/**
 * Agency model events
 */

'use strict';

import {EventEmitter} from 'events';
import Agency from './agency.model';
var AgencyEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AgencyEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Agency.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    AgencyEvents.emit(event + ':' + doc._id, doc);
    AgencyEvents.emit(event, doc);
  };
}

export default AgencyEvents;
